"use client";
import { useEffect } from "react";
import { redirect } from "next/navigation";
import useLocalStorage from "./useLocalStorage";

function useAuth() {
  const [auth, setAuth] = useLocalStorage("auth", undefined);
  useEffect(() => {
    if (!auth) {
      redirect("/");
    }
  }, [auth]);

  return [auth, setAuth];
}

export default useAuth;
