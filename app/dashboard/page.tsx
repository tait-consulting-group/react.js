"use client";
import useAuth from "@/src/hooks/useAuth";
import styles from "./page.module.css";

export default function Dashboard() {
  const [auth, setAuth] = useAuth();

  const logout = () => {
    const response = confirm("Do You Want to Logout");
    if (response) setAuth(undefined);
  };
  return (
    <main className={styles.page}>
      <h1 className={styles.heading}>Hello World</h1>
      <h2 className={styles.caption}>
        Welcome, <span className={styles.email}>{auth?.email}</span>
      </h2>
      <button className="submit" style={{ width: 100 }} onClick={logout}>
        Logout
      </button>
    </main>
  );
}
