"use client";
import { useFormik } from "formik";
import * as Yup from "yup";
import _ from "lodash";
import styles from "./page.module.css";
import useLocalStorage from "@/src/hooks/useLocalStorage";
import { redirect } from "next/navigation";

const inputs = [
  {
    id: "email",
    name: "email",
    type: "email",
  },
  {
    id: "password",
    name: "password",
    type: "password",
  },
];

export default function Home() {
  const [auth, setAuth] = useLocalStorage("auth", undefined);
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .email("Invalid email address")
        .required("Email is required"),
      password: Yup.string()
        .min(8, "Must contain atleast 8 characters")
        .required("Password is required"),
    }),
    onSubmit: (values, { setFieldError }) => {
      if (values.email === "test@test.com" && values.password === "password")
        setAuth({ email: values.email, password: values.password });
      else setFieldError("email", "Incorrect Email or Password");
    },
  });

  if (auth?.email) redirect("/dashboard");

  return (
    <main className={styles.page}>
      <form className={styles.form} onSubmit={formik.handleSubmit}>
        <p className={styles.title}>Login</p>

        {inputs.map((input) => (
          <div className={styles.inputContainer} key={input.id}>
            <label htmlFor={input.name}>
              <span className={styles.labelText}>{_.capitalize(input.id)}</span>
              <input
                className={styles.input}
                id={input.id}
                name={input.name}
                type={input.type}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={(formik.values as any)[input.name]}
              />
            </label>

            {(formik.touched as any)[input.name] &&
            (formik.errors as any)[input.name] ? (
              <span className={styles.errorText}>
                {(formik.errors as any)[input.name]}
              </span>
            ) : null}
          </div>
        ))}
        <button type="submit" className={"submit"}>
          Submit
        </button>
      </form>
    </main>
  );
}
